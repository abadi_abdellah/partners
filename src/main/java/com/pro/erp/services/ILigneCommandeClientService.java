package com.pro.erp.services;

import java.util.List;

import com.pro.erp.entities.LigneCommandeClient;

public interface ILigneCommandeClientService {

	public LigneCommandeClient save(LigneCommandeClient entity);

	public LigneCommandeClient update(LigneCommandeClient entity);

	public List<LigneCommandeClient> selectAll();

	public List<LigneCommandeClient> selectAll(String sortField, String sort);

	public LigneCommandeClient getById(Long id);

	public void remove(Long id);

	public LigneCommandeClient findOne(String paramName, Object paramValue);

	public LigneCommandeClient findOne(String[] paramNames, Object[] paramValues);

	public int founCountBy(String paramName, String paramValue);
}

