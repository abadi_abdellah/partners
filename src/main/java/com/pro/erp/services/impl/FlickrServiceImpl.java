package com.pro.erp.services.impl;

import java.io.InputStream;

import com.pro.erp.dao.IFlickrDao;
import com.pro.erp.services.IFlickrService;

public class FlickrServiceImpl implements IFlickrService {
	
	private IFlickrDao dao;
	
	public void setDao(IFlickrDao dao) {
		this.dao = dao;
	}



	@Override
	public String savePhoto(InputStream strem, String fileName) throws Exception {
		// TODO Auto-generated method stub
		return dao.savePhoto(strem, fileName);
	}

}
