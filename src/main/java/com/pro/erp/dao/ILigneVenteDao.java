package com.pro.erp.dao;

import com.pro.erp.entities.LigneVente;

public interface ILigneVenteDao extends IGenericDao<LigneVente> {

}
