package com.pro.erp.dao;

import com.pro.erp.entities.Category;

public interface ICategoryDao extends IGenericDao<Category> {

}
