package com.pro.erp.dao;

import com.pro.erp.entities.CommandeClient;

public interface ICommandeClientDao extends IGenericDao<CommandeClient> {

}
