package com.pro.erp.dao;

import com.pro.erp.entities.CommandeFournisseur;

public interface ICommandeFournisseurDao extends IGenericDao<CommandeFournisseur> {

}
