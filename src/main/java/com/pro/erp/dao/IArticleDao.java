package com.pro.erp.dao;

import com.pro.erp.entities.Article;

public interface IArticleDao extends IGenericDao<Article> {

}
