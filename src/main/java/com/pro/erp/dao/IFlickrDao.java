package com.pro.erp.dao;

import java.io.InputStream;


public interface IFlickrDao {

	public String savePhoto(InputStream strem, String fileName) throws Exception ;
}
