package com.pro.erp.dao;

import com.pro.erp.entities.Client;

public interface IClientDao extends IGenericDao<Client> {

}
