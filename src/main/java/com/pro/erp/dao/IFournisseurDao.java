package com.pro.erp.dao;

import com.pro.erp.entities.Fournisseur;

public interface IFournisseurDao extends IGenericDao<Fournisseur> {

}
