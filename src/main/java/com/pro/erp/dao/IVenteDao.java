package com.pro.erp.dao;

import com.pro.erp.entities.Vente;

public interface IVenteDao extends IGenericDao<Vente> {

}
