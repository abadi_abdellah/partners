package com.pro.erp.dao;

import com.pro.erp.entities.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur> {

}
