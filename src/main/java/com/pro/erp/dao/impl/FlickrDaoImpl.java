package com.pro.erp.dao.impl;

import java.io.InputStream;

import javax.swing.JOptionPane;

import org.scribe.model.Token;
import org.scribe.model.Verifier;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;

import com.pro.erp.dao.IFlickrDao;

public class FlickrDaoImpl implements IFlickrDao{

	private Flickr flickr;
	
	private UploadMetaData uploadMetaData = new UploadMetaData();
	
	private String apiKey = "7486aab678be0c6bec72275042596f98";
	
	private String sharedSecret = "ae8def73ff530464";
	
	private void connect() {
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		Auth auth = new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("72157680066933978-0e6dd08c32e81246");
		auth.setTokenSecret("46f23a83bbab545f");
		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
		
	}

	@Override
	public String savePhoto(InputStream strem, String fileName) throws Exception{
		connect();
		uploadMetaData.setTitle(fileName);
		String photoId = flickr.getUploader().upload(strem, uploadMetaData);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
	}
	
	
	
	public void auth() {
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		
		AuthInterface authInterface = flickr.getAuthInterface();
		
		Token token = authInterface.getRequestToken();
		System.out.println("token : " + token);

		String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
		System.out.println("Follow this URL to autorise yourself on Filckr");
		System.out.println(url);
		System.out.println("Paste in the token it gives you:");
		System.out.println(">>");
		
		String tokenKey = JOptionPane.showInputDialog(null);
		
		Token requestToken = authInterface.getAccessToken(token, new Verifier(tokenKey));
		System.out.println("Authentification success");
		
		Auth auth = null;
		try {
			auth = authInterface.checkToken(requestToken);
		} catch (FlickrException e) {
			e.printStackTrace();
		}
		
		
		System.out.println("Token : " + requestToken.getToken());
		System.out.println("Secret : " + requestToken.getSecret());
		System.out.println("nsid : " + auth.getUser().getId());
		System.out.println("Realname : " + auth.getUser().getRealName());
		System.out.println("Username : " + auth.getUser().getUsername());
		System.out.println("Permission : " + auth.getPermission().getType());
		
		
	}
	}


